# StraightQuotes

This package will straighten out curly-quotes and other non-ascii characters in a document by replacing them with their ascii equivalents. Curly-quotes are often created by Microsoft Office program's autocorrect. This plugin currently replaces unicode versions of double-quotes, single-quotes, dashes, and more. It does not use HTML entities - instead, it replaces characters with the closest ascii character. 

## What It Does

The current list of replacements is:

* double-quotes ("): “”〃״＂ˮײ᳓″ʺ˝‶‟˶
* single-quotes ('): ‘’՝ߴʹ｀՚ߵ＇ᛌיʾʼʽʻ‛’‘˴ˈˋˊ׳ᑊꞌ′‵᾽᾿΄´῾
* em-dash (-): —ー―ㅡᅳ─一━－⼀㇐ꟷ
* en-dash (-): ––‒‑➖‐⁃−Ⲻ﹘-˗
* bullet (*): ･𐄁•・‧·᛫⸱ᐧ∙⋅
* ellipses (...): …
* trademark (tm): ™
* dagger (+): †
* copyright (C): ©
* registered (R): ®
* cents (cents): ¢
* pound (pounds): £

The list of replacements includes values from the Confusables tool at http://unicode.org.

## Installation

The easiest (and recommended) way to install StraightQuotes is through the Package Control plugin. Download it from https://packagecontrol.io/installation. StraightQuotes is not part of default list of plugins, you can add its repository to Package Control, so once you have Package Control installed, do the following:

1. Open the Command Pallet: `ctrl+shift+p` (Win, Linux) `or cmd+shift+p` (OS X)
2. Select `Package Control: Add Repository`
3. Enter `https://bitbucket.org/rapp-analytics-enablement/straightquotes` to add the repository
4. Select `Package Control: Install Package` from the Command Pallet
5. Select `StraightQuotes` from the list of plugins

Note - you may need to restart Sublime if the menu entries appear grayed out initially. 

## How to Use It

The plugin will install a **StraightQuotes** menu item on both the Edit menu, the right-click context menu, and the command pallet. Click or type one of those. 

*Warning: The replacement is done on the entire document, regardless of your current selection. If you're unhappy with that, Undo and wait for a new version.* 

## Credits

The original idea and implementation of this plugin came from this thread: https://www.sublimetext.com/forum/viewtopic.php?f=3&t=14451. Many thanks to user **qgates** for the code. 

Also to thank is Daryl Tucker, author of the MagiclessQuotes plugin for Sublime (https://github.com/daryltucker/MagiclessQuotes). This never quite worked for us, but his code inspired the addition of more character types to search for. 