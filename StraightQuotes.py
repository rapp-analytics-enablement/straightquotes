#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sublime, sublime_plugin

# StraightQuotes plugin for Sublime Text 
# Homepage: https://github.com/directpartners/StraightQuotes

# original code taken from https://www.sublimetext.com/forum/viewtopic.php?f=3&t=14451
# also thanks to https://github.com/daryltucker/MagiclessQuotes
# This plugin replaces certain unicode characters with ASCII equivalents
# 
# caution - it runs on the entire file, regardless of your current selection

class StraightQuotesCommand(sublime_plugin.TextCommand):
    def run(self, edit):

        replacements = 0

        # curly double-quotes and all characters form the unicode.org confusables page
        # http://unicode.org/cldr/utility/confusables.jsp?a=%22&r=None
        for rgn in self.view.find_all(u"[“”〃״＂ˮײ᳓″ʺ˝‶‟˶]"):
            self.view.replace(edit, rgn, "\"")
            replacements += 1

        # curly single-quotes and all confusables
        # http://unicode.org/cldr/utility/confusables.jsp?a=%27&r=None
        for rgn in self.view.find_all(u"[‘’՝ߴʹ｀՚ߵ＇ᛌיʾʼʽʻ‛’‘˴ˈˋˊ׳ᑊꞌ′‵᾽᾿΄´῾]"):
            self.view.replace(edit, rgn, "'")
            replacements += 1

        # em-dash and confusables
        # http://unicode.org/cldr/utility/confusables.jsp?a=%E2%80%94&r=None
        for rgn in self.view.find_all(u"[—ー―ㅡᅳ─一━－⼀㇐ꟷ]"):
            self.view.replace(edit, rgn, "-")
            replacements += 1

        # en-dash and confusables
        # http://unicode.org/cldr/utility/confusables.jsp?a=-&r=None
        for rgn in self.view.find_all(u"[–‒‑➖‐⁃−Ⲻ﹘˗]"):
            self.view.replace(edit, rgn, "-")
            replacements += 1

        # bullet points and confusables
        # http://unicode.org/cldr/utility/confusables.jsp?a=%E2%80%A2&r=None
        for rgn in self.view.find_all(u"[･𐄁•・‧·᛫⸱ᐧ∙⋅]"):
            self.view.replace(edit, rgn, "*")
            replacements += 1

        # ellipes (no confusables for this one)
        for rgn in self.view.find_all(u"…"):
            self.view.replace(edit, rgn, "...")
            replacements += 1

        # Trademark (no confusables for this one)
        for rgn in self.view.find_all(u"™"):
            self.view.replace(edit, rgn, "(TM)")
            replacements += 1

        # Dagger (no confusables for this one)
        for rgn in self.view.find_all(u"†"):
            self.view.replace(edit, rgn, "+")
            replacements += 1

        # Copyright (no confusables for this one)
        for rgn in self.view.find_all(u"©"):
            self.view.replace(edit, rgn, "(C)")
            replacements += 1

        # Register (no confusables for this one)
        for rgn in self.view.find_all(u"®"):
            self.view.replace(edit, rgn, "(R)")
            replacements += 1

        # Cent (no confusables for this one)
        for rgn in self.view.find_all(u"¢"):
            self.view.replace(edit, rgn, " cents")
            replacements += 1

        # Pound (no confusables for this one)
        for rgn in self.view.find_all(u"£"):
            self.view.replace(edit, rgn, " pounds")
            replacements += 1

        sublime.message_dialog("StraightQuotes made %s replacements" % replacements)